package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.content.Context;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

/**
 * Created by Javi on 6/25/13.
 */
public class WebService {

    private static final String ROUTE = "http://192.168.1.146";
    private static final String PORT = "3000";
    private static final String HEADER_NAME = "content-type";
    private static final String HEADER_VALUE = "application/json";

    public static JSONArray SendGetRequest(Context ctx, String path)
    {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(ROUTE + ":" + PORT + path);
        request.setHeader(HEADER_NAME, HEADER_VALUE);

        try
        {
            HttpResponse response = client.execute(request);
            String respStr = EntityUtils.toString(response.getEntity());

            return new JSONArray(respStr);
        }
        catch(Exception ex)
        {
            Toast.makeText(ctx, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return null;
    }
}
