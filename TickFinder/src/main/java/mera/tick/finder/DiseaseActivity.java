package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class DiseaseActivity extends FragmentActivity implements View.OnTouchListener{

    private DiseaseClass disease;
    private List<Button> buttonList = new ArrayList<Button>();

    private Button btnDescription;
    private Button btnSymptom;
    private Button btnTreatment;
    private Button btnDiagnosis;

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    private FragmentDescription fragmentDescription;
    private FragmentSymptom fragmentSymptom;
    private FragmentTreatment fragmentTreatment;
    private FragmentDiagnosis fragmentDiagnosis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.disease_activity);
        this.fragmentManager = getSupportFragmentManager();
        this.disease = getIntent().getParcelableExtra("diseaseSelected");
        this.setTitle(this.disease.getName());

        this.btnDescription = (Button) this.findViewById(R.id.btn_description);
        this.btnDescription.setClickable(false);
        this.btnDescription.setOnTouchListener(this);
        this.btnDescription.setBackgroundResource(R.color.MyRed);

        this.btnSymptom = (Button) this.findViewById(R.id.btn_symptom);
        this.btnSymptom.setOnTouchListener(this);

        this.btnTreatment = (Button) this.findViewById(R.id.btn_treatment);
        this.btnTreatment.setOnTouchListener(this);

        this.btnDiagnosis = (Button) this.findViewById(R.id.btn_Diagnosis);
        this.btnDiagnosis.setOnTouchListener(this);

        this.buttonList.add(this.btnDescription);
        this.buttonList.add(this.btnSymptom);
        this.buttonList.add(this.btnTreatment);
        this.buttonList.add(this.btnDiagnosis);

        this.fragmentDescription = FragmentDescription.newInstance(this.disease.getDescription(), this.disease.getWhereFound(), this.disease.getIncubationPeriod());
        this.fragmentSymptom = FragmentSymptom.newInstance(this.disease.getSymptom());
        this.fragmentTreatment = FragmentTreatment.newInstance(this.disease.getTreatment(), this.disease.getTreatmentLength());
        this.fragmentDiagnosis = FragmentDiagnosis.newInstance(this.disease.getDiagnosis());

        this.fragmentTransaction = fragmentManager.beginTransaction();
        this.fragmentTransaction.replace(R.id.diseaseContainer,this.fragmentDescription);
        this.fragmentTransaction.commit();
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        for(Button btn: this.buttonList)
        {
            btn.setBackgroundColor(Color.WHITE);
        }

        switch(view.getId())
        {
            case R.id.btn_description:

                this.fragmentTransaction = fragmentManager.beginTransaction();
                this.fragmentTransaction.replace(R.id.diseaseContainer,this.fragmentDescription);
                this.fragmentTransaction.commit();

                break;

            case R.id.btn_symptom:

                this.fragmentTransaction = fragmentManager.beginTransaction();
                this.fragmentTransaction.replace(R.id.diseaseContainer, this.fragmentSymptom);
                this.fragmentTransaction.commit();

                break;

            case R.id.btn_treatment:

                this.fragmentTransaction = fragmentManager.beginTransaction();
                this.fragmentTransaction.replace(R.id.diseaseContainer,this.fragmentTreatment);
                this.fragmentTransaction.commit();

                break;
            case R.id.btn_Diagnosis:

                this.fragmentTransaction = fragmentManager.beginTransaction();
                this.fragmentTransaction.replace(R.id.diseaseContainer, this.fragmentDiagnosis);
                this.fragmentTransaction.commit();
        }

        ((Button)view).setBackgroundResource(R.color.MyRed);
        ((Button)view).setClickable(false);

        return true;
    }
}