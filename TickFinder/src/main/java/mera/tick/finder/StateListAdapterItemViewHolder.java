package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class StateListAdapterItemViewHolder {

    public int viewId;
    public ImageView stateFlag;
    public TextView stateName;
    public CheckBox stateSelected;
}
