package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Javi on 6/21/13.
 */
public class ExpandableViewAdapter extends BaseExpandableListAdapter {

    private StateClass state;
    private LayoutInflater inflater;
    private SelectedStateFragment parent;

    public ExpandableViewAdapter(Context ctx, StateClass state, SelectedStateFragment parent)
    {
        this.inflater = LayoutInflater.from(ctx);
        this.state = state;
        this.parent = parent;
    }

    @Override
    public int getGroupCount() {
        return this.state.getAllDiseases().length;
    }

    @Override
    public int getChildrenCount(int i) {
        return this.state.getDisease(i).getAllTicks().length;
    }

    @Override
    public DiseaseClass getGroup(int i) {
        return this.state.getDisease(i);
    }

    @Override
    public TickClass getChild(int i, int i2) {
        return this.state.getDisease(i).getTick(i2);
    }

    @Override
    public long getGroupId(int i) {
        return this.state.getDisease(i).getId();
    }

    @Override
    public long getChildId(int i, int i2) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup viewGroup) {

        ExpandableGroupHolder holder = null;

        if(!(view instanceof ViewGroup))
        {
            holder = new ExpandableGroupHolder();

            view = this.inflater.inflate(R.layout.expandable_parent, viewGroup, false);

            holder.diseaseTitle = (TextView) view.findViewById(R.id.DiseaseName);

            holder.diseaseIncidence = (TextView) view.findViewById(R.id.IncidenceValue);
            holder.diseaseButtonSearch = (Button) view.findViewById(R.id.DiseaseButton);

            holder.diseaseButtonSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ExpandableGroupHolder eventHolder = (ExpandableGroupHolder) ((ViewGroup)view.getParent()).getTag();
                    parent.SearchDisease(eventHolder.diseaseId);
                }
            });

            view.setTag(holder);
        }
        else
        {
            holder = (ExpandableGroupHolder) view.getTag();
        }

        holder.diseaseTitle.setText(this.state.getDisease(groupPosition).getName());
        holder.diseaseIncidence.setText(this.state.getIncidence(groupPosition));
        holder.diseaseIncidence.setTextColor(DiseaseIncidenceColor.IncidenceColor[this.state.getDisease(groupPosition).getColorId()]);

        if(isExpanded)
        {
            holder.diseaseId = (int) getGroupId(groupPosition);
        }
        else
        {
            holder.diseaseId = groupPosition;
        }

        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean b, View view, ViewGroup viewGroup) {
        ExpandableChildHolder holder = null;

        if(!(view instanceof  ViewGroup))
        {
            holder = new ExpandableChildHolder();

            view = this.inflater.inflate(R.layout.expandable_child, viewGroup, false);

            try
            {
                holder.tickImage = (ImageView) view.findViewById(R.id.tickImage);
                holder.tickName = (TextView) view.findViewById(R.id.tickName);
                holder.tickSciName = (TextView) view.findViewById(R.id.tickSciName);
            }
            catch(NullPointerException ex)
            {
                Toast.makeText(this.inflater.getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            catch(RuntimeException ex)
            {
                Toast.makeText(this.inflater.getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
            }

            view.setTag(holder);
        }
        else
        {
            holder = (ExpandableChildHolder) view.getTag();
        }

        TickClass tick = getGroup(groupPosition).getTick(childPosition);

        holder.tickImage.setImageResource(tick.GetTickImage());
        holder.tickName.setText(tick.GetTickName());
        holder.tickSciName.setText(tick.GetTickScientificName());

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
