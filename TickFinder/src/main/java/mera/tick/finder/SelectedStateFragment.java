package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SelectedStateFragment extends Fragment {

    private int fragmentId;
    private View fragmentView;
    private TextView stateName;
    private StateClass state;
    private ExpandableListView expandableList;
    private ExpandableViewAdapter adapter;

    public static final SelectedStateFragment newInstance(int id, StateClass state)
    {
        SelectedStateFragment myFragment = new SelectedStateFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        bundle.putParcelable("STATE_SELECTED", state);
        myFragment.setArguments(bundle);

        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.fragmentView = inflater.inflate(R.layout.selected_state, container, false);
        this.stateName = (TextView) this.fragmentView.findViewById(R.id.stateName);
        this.expandableList = (ExpandableListView) this.fragmentView.findViewById(R.id.expandableList);

        return this.fragmentView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.fragmentId = getArguments().getInt("id");
        this.state = getArguments().getParcelable("STATE_SELECTED");
        this.stateName.setText(this.state.getName());
        this.CheckFragmentId();
        this.adapter = new ExpandableViewAdapter(getActivity(), this.state, this);
        this.expandableList.setBackgroundColor(Color.WHITE);
        this.expandableList.setCacheColorHint(Color.WHITE);

        this.expandableList.setAdapter(this.adapter);
        this.expandableList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long l) {
                ViewTickTickInformation(state.getDisease(i).getTick(i2));
                return false;
            }
        });
    }

    private void CheckFragmentId()
    {
        if(this.fragmentId == 0)
        {
            if(this.fragmentId == SearchResultViewPagerAdapter.SIZE - 1)
            {
                this.SetRightArrowInvisible();
            }

            this.SetLeftArrowInvisible();
        }
        else if(this.fragmentId == SearchResultViewPagerAdapter.SIZE - 1)
        {
            this.SetRightArrowInvisible();
        }
    }

    private void SetRightArrowInvisible()
    {
        ImageView iv = (ImageView) this.fragmentView.findViewById(R.id.RightArrow);
        iv.setVisibility(View.INVISIBLE);
    }

    private void SetLeftArrowInvisible()
    {
        ImageView iv = (ImageView) this.fragmentView.findViewById(R.id.LeftArrow);
        iv.setVisibility(View.INVISIBLE);
    }

    public void SearchDisease(int diseaseId)
    {
        SearchResultActivity act = (SearchResultActivity) this.getActivity();
        act.LaunchDiseaseActivity(this.fragmentId, diseaseId);
    }

    public void ViewTickTickInformation(TickClass tick)
    {
        SearchResultActivity act = (SearchResultActivity) this.getActivity();
        act.LaunchTickActivity(tick);
    }
}