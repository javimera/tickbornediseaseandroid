package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.os.Parcel;
import android.os.Parcelable;

public class TickClass implements Parcelable {

    private String tickName;
    private String tickScientificName;
    private String comments;
    private String whereFound;
    private int tickImageRes;
    private int tickDistributionImageRes;

    public static final String TABLE = "ticks";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_SCI_NAME = "scientificname";
    public static final String COLUMN_COMMENTS = "comments";
    public static final String COLUMN_WHERE_FOUND = "where_found";

    public static final String CREATE_STRING =
            "CREATE TABLE " + TABLE + "(" +
                    AppDB.COLUMN_ID + " INTEGER PRIMARY KEY autoincrement," +
                    COLUMN_NAME + " TEXT," +
                    COLUMN_SCI_NAME + " TEXT," +
                    COLUMN_COMMENTS + " TEXT," +
                    COLUMN_WHERE_FOUND + " TEXT)";


    public TickClass(String name, String sciname, int tickImg, int tickDistributionImg, String comments, String where)
    {
        this.tickName = name;
        this.tickScientificName = sciname;
        this.comments = comments;
        this.whereFound = where;
        this.tickImageRes = tickImg;
        this.tickDistributionImageRes = tickDistributionImg;
    }

    public TickClass(Parcel parcel) {
        this.tickName = parcel.readString();
        this.tickScientificName = parcel.readString();
        this.comments = parcel.readString();
        this.whereFound = parcel.readString();
        this.tickImageRes = parcel.readInt();
        this.tickDistributionImageRes = parcel.readInt();
    }

    public String GetTickName()
    {
        return this.tickName;
    }

    public String GetTickScientificName()
    {
        return this.tickScientificName;
    }

    public String GetComments()
    {
        return this.comments;
    }

    public int GetTickImage()
    {
        return this.tickImageRes;
    }

    public int GetTickDestributionImage()
    {
        return this.tickDistributionImageRes;
    }

    public String GetWhereFound()
    {
        return this.whereFound;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(this.tickName);
        parcel.writeString(this.tickScientificName);
        parcel.writeString(this.comments);
        parcel.writeString(this.whereFound);
        parcel.writeInt(this.tickImageRes);
        parcel.writeInt(this.tickDistributionImageRes);
    }

    public static final Parcelable.Creator<TickClass> CREATOR = new Parcelable.Creator<TickClass>()
    {
        @Override
        public TickClass createFromParcel(Parcel parcel) {
            return new TickClass(parcel);
        }

        @Override
        public TickClass[] newArray(int i) {
            return new TickClass[i];
        }
    };
}