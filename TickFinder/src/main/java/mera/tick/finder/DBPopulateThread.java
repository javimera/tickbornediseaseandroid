package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.app.Activity;
import android.app.AlertDialog;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

public class DBPopulateThread extends AsyncTask<Void, Integer, Void> {

    private Activity act;
    private AlertDialog dialog;
    private Fragment fragment;
    private boolean update;

    public DBPopulateThread(Activity act, Fragment f, boolean needsUpdate)
    {
        this.act = act;
        this.fragment = f;
        this.update = needsUpdate;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.dialog = new AlertDialog.Builder(this.act).create();
        this.dialog.setTitle("Connecting to server");

        if(this.update)
            this.dialog.setMessage("Updating application database...Wait a moment");
        else
        {
            this.dialog.setMessage("Loading data into database application...");
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {

        publishProgress(1);

        AppDB myDB = new AppDB(this.act, ((LoadingFragment)fragment).getDbVersion());
        myDB.InsertStates(WebService.SendGetRequest(this.act, "//states.json"));
        myDB.InsertDiseases(WebService.SendGetRequest(this.act, "//diseases.json"));
        myDB.InsertIncidence(WebService.SendGetRequest(this.act, "//incidences.json"));
        myDB.InsertTicks(WebService.SendGetRequest(this.act, "//ticks.json"));

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);

        if(!this.dialog.isShowing())
        {
            this.dialog.show();
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        this.dialog.dismiss();
        new LoadingThread(this.fragment).execute();
    }
}