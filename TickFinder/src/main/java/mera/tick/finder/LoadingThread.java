package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.annotation.TargetApi;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

public class LoadingThread extends AsyncTask<Void, Integer, Void>
{
    private StateClass[] states;
    private DiseaseClass[] diseases = null;
    private TickClass[] ticks = null;

    private Fragment fragment;

    int barProgress;

    public LoadingThread(Fragment fragment)
    {
        this.fragment = fragment;
    }

    @Override
    protected Void doInBackground(Void... voids)
    {
        this.GetTicks();
        this.GetDiseases();

        this.GetStates();

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        // initialize bar progress
        barProgress = 0;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        this.ReplaceFragment();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        ((ProgressBar)this.fragment.getView().findViewById(R.id.loadingBar)).setProgress(values[0]);
    }

    private void GetStates()
    {
        SQLiteDatabase db = new AppDB(this.fragment.getActivity(), ((LoadingFragment)this.fragment).getDbVersion()).getReadableDatabase();
        Cursor cursorStates = db.query(StateClass.TABLE, new String[]{AppDB.COLUMN_ID, StateClass.COLUMN_NAME}, null, null, null, null, null);

        try
        {
            if(cursorStates != null)
            {
                cursorStates.moveToFirst();
                this.states = new StateClass[cursorStates.getCount()];

                for(int i = 0 ; i < this.states.length ; i++)
                {
                    Cursor cursorIncidences = db.query(
                            IncidenceClass.TABLE,
                            new String[]{IncidenceClass.COLUMN_ID_DISEASE, IncidenceClass.COLUMN_VALUE},
                            IncidenceClass.COLUMN_ID_STATE + "=?",
                            new String[]{String.valueOf(cursorStates.getInt(0))},
                            null,
                            null,
                            null,
                            null
                    );

                    if(cursorIncidences != null)
                    {
                        cursorIncidences.moveToFirst();
                        DiseaseClass[] diseaseArray = new DiseaseClass[cursorIncidences.getCount()];
                        String[] incidenceArray = new String[cursorIncidences.getCount()];

                        for(int d = 0 ; d < diseaseArray.length ; d++)
                        {
                            diseaseArray[d] = new DiseaseClass(
                                    this.diseases[cursorIncidences.getInt(0) - 1].getName(),
                                    this.diseases[cursorIncidences.getInt(0) - 1].getDescription(),
                                    this.diseases[cursorIncidences.getInt(0) - 1].getSymptom(),
                                    this.diseases[cursorIncidences.getInt(0) - 1].getTreatment(),
                                    d,
                                    this.diseases[cursorIncidences.getInt(0) - 1].getAllTicks(),
                                    this.diseases[cursorIncidences.getInt(0) - 1].getTreatmentLength(),
                                    this.diseases[cursorIncidences.getInt(0) - 1].getDiagnosis(),
                                    this.diseases[cursorIncidences.getInt(0) - 1].getWhereFound(),
                                    this.diseases[cursorIncidences.getInt(0) - 1].getIncubationPeriod(),
                                    this.diseases[cursorIncidences.getInt(0) - 1].getColorId());

                            incidenceArray[d] = cursorIncidences.getString(1);

                            cursorIncidences.moveToNext();
                        }

                        this.states[i] = new StateClass(
                                cursorStates.getInt(0) - 1,
                                false,
                                cursorStates.getString(1),
                                incidenceArray,
                                diseaseArray
                        );
                    }

                    barProgress += 2;
                    publishProgress(barProgress);

                    cursorIncidences.close();
                    cursorStates.moveToNext();
                }

                cursorStates.close();
            }
        }
        catch(NullPointerException ex)
        {
        }
        finally
        {
            db.close();
        }
    }

    private void GetDiseases()
    {
        SQLiteDatabase db = new AppDB(this.fragment.getActivity(), ((LoadingFragment)this.fragment).getDbVersion()).getReadableDatabase();
        Cursor cursor = db.query(
                DiseaseClass.TABLE,
                new String[]{
                        DiseaseClass.COLUMN_NAME,
                        DiseaseClass.COLUMN_DESCRIPTION,
                        DiseaseClass.COLUMN_SYMPTOMS,
                        DiseaseClass.COLUMN_TREATMENT,
                        DiseaseClass.COLUMN_TICKS,
                        DiseaseClass.COLUMN_LENGTH,
                        DiseaseClass.COLUMN_DIAGNOSIS,
                        DiseaseClass.COLUMN_WHERE_FOUND,
                        DiseaseClass.COLUMN_INCUBATION_PERIOD,
                        DiseaseClass.COLUMN_INCIDENCE_COLOR},
                null,
                null,
                null,
                null,
                null);

        if(cursor != null)
        {
            cursor.moveToFirst();
            this.diseases = new DiseaseClass[cursor.getCount()];

            for(int d = 0 ; d < diseases.length ; d++)
            {
                String[] tickIds = cursor.getString(4).split(",");
                TickClass[] tickObjects = new TickClass[tickIds.length];

                for(int j = 0 ; j < tickIds.length ; j++ )
                {
                    String index = tickIds[j];

                    tickObjects[j] = new TickClass(
                            this.ticks[Integer.parseInt(index)- 1].GetTickName(),
                            this.ticks[Integer.parseInt(index)- 1].GetTickScientificName(),
                            this.ticks[Integer.parseInt(index)- 1].GetTickImage(),
                            Integer.parseInt(index) - 1,
                            this.ticks[Integer.parseInt(index) - 1].GetComments(),
                            this.ticks[Integer.parseInt(index) - 1].GetWhereFound()
                    );
                }

                diseases[d] = new DiseaseClass(
                        cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        d,
                        tickObjects,
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getInt(9)
                );

                cursor.moveToNext();
            }
        }

        cursor.close();
        db.close();
    }

    private void GetTicks() {

        SQLiteDatabase db = new AppDB(this.fragment.getActivity(), ((LoadingFragment)this.fragment).getDbVersion()).getReadableDatabase();
        Cursor cursor = db.query(
                TickClass.TABLE,
                new String[]{
                        TickClass.COLUMN_NAME,
                        TickClass.COLUMN_SCI_NAME,
                        TickClass.COLUMN_COMMENTS,
                        TickClass.COLUMN_WHERE_FOUND
                },
                null,
                null,
                null,
                null,
                null);

        try
        {
            if(cursor != null)
            {
                cursor.moveToFirst();
                this.ticks = new TickClass[cursor.getCount()];

                for(int i = 0 ; i < this.ticks.length ; i++)
                {
                    this.ticks[i] = new TickClass(
                            cursor.getString(0),
                            cursor.getString(1),
                            TickClassImages.tickImages[i],
                            TickClassImages.tickDistributionMap[i],
                            cursor.getString(2),
                            cursor.getString(3));

                    cursor.moveToNext();
                }
            }
        }
        catch(NullPointerException ex)
        {
            Log.e("tickapp", ex.getMessage());
        }

        cursor.close();
        db.close();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCancelled(Void aVoid) {
        super.onCancelled(aVoid);
        Toast.makeText(this.fragment.getActivity(), "Cancelled operation", Toast.LENGTH_SHORT).show();
    }

    private void ReplaceFragment()
    {
        FragmentTransaction fragmentTransaction = this.fragment.getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, StateListFragment.newInstance(this.states));
        fragmentTransaction.commit();
    }
}
