package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ExpandableChildHolder {

    public ImageView tickImage;
    public TextView tickName;
    public TextView tickSciName;
    public int tickDistributionImageId;
}
