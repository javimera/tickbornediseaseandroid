package mera.tick.finder;

import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Javi on 7/3/13.
 */
public class ExpandableGroupHolder {

    public TextView diseaseTitle;
    public TextView diseaseIncidence;
    public Button diseaseButtonSearch;
    public int diseaseId;
}