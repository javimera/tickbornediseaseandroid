package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Javi on 6/22/13.
 */
public class FragmentDescription extends Fragment {

    private View fragmentView;
    private TextView descriptionView;
    private TextView whereFoundTextView;
    private TextView incubationPeriodTextView;

    public static FragmentDescription newInstance(String description, String where, String incubation)
    {
        FragmentDescription myFragment = new FragmentDescription();
        Bundle bundle = new Bundle();
        bundle.putString("where", where);
        bundle.putString("incubation", incubation);
        bundle.putString("description", description);
        myFragment.setArguments(bundle);

        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.fragmentView = inflater.inflate(R.layout.fragment_description, container, false);
        this.descriptionView = (TextView) this.fragmentView.findViewById(R.id.descriptionDisease);
        this.whereFoundTextView = (TextView) this.fragmentView.findViewById(R.id.whereFoundDisease);
        this.incubationPeriodTextView = (TextView) this.fragmentView.findViewById(R.id.incubationPeriodDisease);

        return this.fragmentView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        createSpannableText(this.whereFoundTextView, "Where Found:", this.getArguments().getString("where"));
        createSpannableText(this.incubationPeriodTextView, "Incubation Period:", this.getArguments().getString("incubation"));
        createSpannableText(this.descriptionView, "Description:", this.getArguments().getString("description"));
    }

    private void createSpannableText(TextView view, String title, String text)
    {
        SpannableStringBuilder spannable = new SpannableStringBuilder(title);
        spannable.setSpan(new StyleSpan(Typeface.BOLD),0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.append("\n\n");
        spannable.append(text);
        view.append(spannable);
    }
}