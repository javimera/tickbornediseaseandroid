package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class StateListAdapter extends BaseAdapter implements View.OnClickListener{

    private StateClass[] stateArray;
    private LayoutInflater inflater;

    public StateListAdapter(StateClass[] states, Context ctx)
    {
        this.stateArray = states;
        this.inflater = LayoutInflater.from(ctx);
    }

    @Override
    public int getCount() {
        return this.stateArray.length;
    }

    @Override
    public StateClass getItem(int i) {
        return this.stateArray[i];
    }

    @Override
    public long getItemId(int i) {
        return this.stateArray[i].getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        StateListAdapterItemViewHolder holder = null;

        if(!(view instanceof ViewGroup))
        {
            holder = new StateListAdapterItemViewHolder();
            view = this.inflater.inflate(R.layout.statelist_item, viewGroup, false);

            holder.stateFlag = (ImageView) view.findViewById(R.id.flag);
            holder.stateName = (TextView) view.findViewById(R.id.itemName);
            holder.stateSelected = (CheckBox) view.findViewById(R.id.itemBox);

            holder.stateSelected.setOnClickListener(this);

            view.setTag(holder);
        }
        else
        {
            holder = (StateListAdapterItemViewHolder) view.getTag();
        }

        holder.viewId = this.stateArray[i].getId();
        holder.stateFlag.setImageResource(StateFlags.Id[i]);
        holder.stateName.setText(this.stateArray[i].getName());
        holder.stateSelected.setChecked(this.stateArray[i].IsChecked());

        return view;
    }

    @Override
    public void onClick(View view) {
        StateListAdapterItemViewHolder eventHolder = (StateListAdapterItemViewHolder) ((ViewGroup)view.getParent()).getTag();
        stateArray[eventHolder.viewId].setChecked(eventHolder.stateSelected.isChecked());
    }
}