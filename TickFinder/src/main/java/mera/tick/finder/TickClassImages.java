package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */

public class TickClassImages {
    public static int[] tickImages = new int[]
            {
                    R.drawable.americandogtick,
                    R.drawable.blacklegged_tick,
                    R.drawable.browndogtick,
                    R.drawable.gulfcoasttick,
                    R.drawable.lonestartick,
                    R.drawable.rockymountainwoodtick,
                    R.drawable.westernblackleggedtick
            };

    public static int[] tickDistributionMap = new int[]
            {
                    R.drawable.americandogtickmap,
                    R.drawable.blackleggedtickmap,
                    R.drawable.browndogtickmap,
                    R.drawable.gulfcoasttickmap,
                    R.drawable.lonestartickmap,
                    R.drawable.rockymountainwoodtickmap,
                    R.drawable.westernblackleggedtickmap
            };
}
