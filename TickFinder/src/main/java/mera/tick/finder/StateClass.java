package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.os.Parcel;
import android.os.Parcelable;

public class StateClass implements Parcelable{

    private int id;
    private boolean isSelected;
    private String name;
    private String[] incidences;
    private DiseaseClass[] diseases;

    public static final String TABLE = "states";
    public static final String COLUMN_NAME = "name";

    public static final String CREATE_STRING =
            "CREATE TABLE " + TABLE + "(" +
                    AppDB.COLUMN_ID + " INTEGER PRIMARY KEY autoincrement," +
                    COLUMN_NAME + " TEXT)";

    public StateClass(int id, boolean checked, String name, String[] incidences, DiseaseClass[] diseases)
    {
        this.id = id;
        this.isSelected = checked;
        this.name = name;
        this.incidences = incidences;
        this.diseases = diseases;
    }

    public StateClass(Parcel parcel)
    {
        this.id = parcel.readInt();
        this.isSelected = parcel.readByte() == 1;
        this.name = parcel.readString();

        this.incidences = new String[parcel.readInt()];

        parcel.readStringArray(this.incidences);

        this.diseases = new DiseaseClass[parcel.readInt()];

        for(int d = 0 ; d < this.diseases.length ; d++)
        {
            this.diseases[d] = DiseaseClass.CREATOR.createFromParcel(parcel);
        }
    }

    public int getId()
    {
        return this.id;
    }

    public boolean IsChecked()
    {
        return this.isSelected;
    }

    public void setChecked(boolean b)
    {
        this.isSelected = b;
    }

    public String getName()
    {
        return this.name;
    }

    public String getIncidence(int position)
    {
        return this.incidences[position];
    }

    public String[] getAllIncidences()
    {
        return this.incidences;
    }

    public DiseaseClass getDisease(int position)
    {
        return this.diseases[position];
    }

    public DiseaseClass[] getAllDiseases()
    {
        return this.diseases;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeInt(this.id);
        parcel.writeByte((byte)(this.isSelected ? 1 : 0));
        parcel.writeString(this.name);
        parcel.writeInt(this.incidences.length);
        parcel.writeStringArray(this.incidences);
        parcel.writeInt(this.diseases.length);

        for(int d = 0 ; d < this.diseases.length ; d++)
        {
            this.diseases[d].writeToParcel(parcel, i);
        }
    }

    public static final Parcelable.Creator<StateClass> CREATOR = new Parcelable.Creator<StateClass>()
    {
        @Override
        public StateClass createFromParcel(Parcel parcel) {
            return new StateClass(parcel);
        }

        @Override
        public StateClass[] newArray(int i) {
            return new StateClass[i];
        }
    };
}
