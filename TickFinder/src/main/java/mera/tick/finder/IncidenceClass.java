package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */

public class IncidenceClass {

    public static final String TABLE = "incidences";
    public static final String COLUMN_ID_STATE = "state_id";
    public static final String COLUMN_ID_DISEASE = "disease_id";
    public static final String COLUMN_VALUE = "value";

    public static final String CREATE_STRING =
            "CREATE TABLE " + TABLE + "(" +
                    AppDB.COLUMN_ID + " INTEGER PRIMARY KEY autoincrement," +
                    COLUMN_ID_DISEASE + " INTEGER," +
                    COLUMN_ID_STATE + " INTEGER," +
                    COLUMN_VALUE + " TEXT)";
}
