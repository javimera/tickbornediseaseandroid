package mera.tick.finder;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

/**
 * Created by Javi on 7/25/13.
 */
public class TickMapActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.tickmap_activity);

        ((ImageView)this.findViewById(R.id.tick_map)).setImageResource(TickClassImages.tickDistributionMap[this.getIntent().getIntExtra("mapid",0)]);
    }
}

