package mera.tick.finder;

import android.graphics.Color;

/**
 * Created by Javi on 7/25/13.
 */
public class DiseaseIncidenceColor {

    public static final int[] IncidenceColor = new int[]
            {
                Color.parseColor("#FF6666"),
                Color.BLUE,
                Color.parseColor("#008000")
            };

    public static final String[] IncidenceColorText = new String[]
            {
                "Incidence of reported cases per million persons in 2010",
                "Incidence of reported cases per 100,000 persons in 2011",
                "Total number of cases reported from 2001-2010"
            };
}
