package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class FragmentTreatment extends Fragment implements View.OnClickListener{

    private String[] treatment;
    private String[] treatmentLength;
    private View fragmentView;
    private Button treatmentLengthButton;

    public static FragmentTreatment newInstance(String treatment, String length)
    {
        FragmentTreatment myFragment = new FragmentTreatment();
        Bundle bundle = new Bundle();
        bundle.putStringArray("treatment", treatment.split(";"));
        bundle.putStringArray("length", length.split(";"));

        myFragment.setArguments(bundle);

        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.fragmentView = inflater.inflate(R.layout.fragment_treatment, container, false);
        this.treatmentLengthButton = (Button) this.fragmentView.findViewById(R.id.btn_length);
        this.treatmentLengthButton.setOnClickListener(this);
        return this.fragmentView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.treatment = getArguments().getStringArray("treatment");
        this.treatmentLength = getArguments().getStringArray("length");

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        LinearLayout parentLinearLayout = (LinearLayout) this.fragmentView.findViewById(R.id.treatmentLayout);


        for(int i = 0 ; i < this.treatment.length; i++)
        {
            String[] itemDescription = this.treatment[i].split("-");

            View childLinearLayout = inflater.inflate(R.layout.fragment_treatment_item, null);
            TextView parent = (TextView) childLinearLayout.findViewById(R.id.treatmentDescription);

            if(i == this.treatment.length - 1)
            {
                parent.setTypeface(null, Typeface.BOLD);

                for(int j = 0 ; j < itemDescription.length ; j++)
                {
                    parent.append(itemDescription[j] + "\n\n");
                }
            }
            else
            {
                parent.setText(itemDescription[0]);

                for(int j = 1 ; j < itemDescription.length ; j++)
                {
                    parent.append( "\n\n" + "-" + itemDescription[j]);
                }
            }
            parentLinearLayout.addView(childLinearLayout);
        }
    }

    @Override
    public void onClick(View view) {
        AlertDialog dialog = new AlertDialog.Builder(this.getActivity()).create();
        dialog.setTitle("Duration of treatment (Days)");
        String message = "";

        for(int tl = 0 ; tl < this.treatmentLength.length ; tl++)
        {
            message += this.treatmentLength[tl] + "\n\n";
        }

        dialog.setMessage(message);
        dialog.show();
    }
}