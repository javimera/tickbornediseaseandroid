package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

public class SearchResultActivity extends FragmentActivity {

    private StateClass[] statesChosen;
    private ViewPager pager;
    private SearchResultViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);

        this.InitializeStateArray();
        this.pager = (ViewPager) this.findViewById(R.id.pager);
        this.adapter = new SearchResultViewPagerAdapter(getSupportFragmentManager(),this.statesChosen);
        this.pager.setAdapter(this.adapter);

        ((TextView)this.findViewById(R.id.red_incidence)).setText(DiseaseIncidenceColor.IncidenceColorText[0]);
        ((TextView)this.findViewById(R.id.blue_incidence)).setText(DiseaseIncidenceColor.IncidenceColorText[1]);
        ((TextView)this.findViewById(R.id.green_incidence)).setText(DiseaseIncidenceColor.IncidenceColorText[2]);
    }

    private void InitializeStateArray()
    {
        this.statesChosen = new StateClass[getIntent().getIntExtra("howMany", 0)];

        for(int s = 0 ; s < this.statesChosen.length ; s++)
        {
            this.statesChosen[s] = getIntent().getExtras().getParcelable("state" + s);
        }
    }

    public void LaunchDiseaseActivity(int state, int diseaseId) {

        Intent intent = new Intent(this, DiseaseActivity.class);
        intent.putExtra("diseaseSelected", this.statesChosen[state].getDisease(diseaseId));

        startActivity(intent);
    }

    public void LaunchTickActivity(TickClass tick)
    {
        Intent intent = new Intent(this, TickActivity.class);
        intent.putExtra("tick", tick);

        startActivity(intent);
    }
}