package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class FragmentSymptom extends Fragment {

    private FragmentSymptomListAdapter adapter;
    private View fragmentView;
    private TextView symptomNoteTextView;
    private Spinner spinner;
    private ArrayAdapter<String> spinnerAdapter;
    private ListView list;
    private List<List<String>> items = new ArrayList<List<String>>();

    public static FragmentSymptom newInstance(String symptoms)
    {
        FragmentSymptom myFragment = new FragmentSymptom();
        Bundle bundle = new Bundle();
        bundle.putStringArray("symptoms", symptoms.split(";"));

        myFragment.setArguments(bundle);

        return myFragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String[] symptomItems = this.getArguments().getStringArray("symptoms");
        ArrayList<String> spinnerItems = new ArrayList<String>();

        for(int i = 0 ; i < symptomItems.length - 1 ; i++)
        {
            List<String> listOfSymptoms = new ArrayList<String>();
            String[] symptom = symptomItems[i].split("-");
            spinnerItems.add(symptom[0]);

            for(int j = 1 ; j < symptom.length ; j++)
            {
                listOfSymptoms.add(symptom[j]);
            }

            this.items.add(listOfSymptoms);
        }

        this.spinnerAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, spinnerItems);
        this.spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        this.spinner.setAdapter(this.spinnerAdapter);

        this.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                list.setAdapter(null);
                adapter = new FragmentSymptomListAdapter(getActivity(), items.get(i));
                list.setAdapter(adapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        this.list.setAdapter(this.adapter);
        this.list.setBackgroundColor(Color.WHITE);
        this.list.setCacheColorHint(Color.WHITE);
        this.list.setClickable(false);

        String[] symptomNotes = symptomItems[symptomItems.length - 1].split("-");

        for(int i = 0 ; i < symptomNotes.length ; i++)
        {
            this.symptomNoteTextView.append(symptomNotes[i] + "\n\n");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.fragmentView = inflater.inflate(R.layout.fragment_symptoms, container, false);
        this.list = (ListView) this.fragmentView.findViewById(R.id.symptomsList);
        this.spinner = (Spinner) this.fragmentView.findViewById(R.id.spinner);
        this.symptomNoteTextView = (TextView) this.fragmentView.findViewById(R.id.symptomNote);

        return this.fragmentView;
    }
}