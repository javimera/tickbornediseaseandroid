package mera.tick.finder;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Javi on 7/19/13.
 */
public class FragmentDiagnosis extends Fragment {

    private View fragmentView;
    private TextView diagnosisIntroTextView;
    private TextView diagnosisItemsTextView;
    private String[] diagnosisItems;

    public static FragmentDiagnosis newInstance(String diagnosis)
    {
        FragmentDiagnosis myFragment = new FragmentDiagnosis();
        Bundle bundle = new Bundle();
        bundle.putString("diagnosisItems",diagnosis);
        myFragment.setArguments(bundle);

        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.fragmentView = inflater.inflate(R.layout.fragment_diagnosis, container, false);
        this.diagnosisIntroTextView = (TextView) this.fragmentView.findViewById(R.id.diagnosisIntro);
        this.diagnosisItemsTextView = (TextView) this.fragmentView.findViewById(R.id.diagnosisItems);

        return this.fragmentView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.diagnosisItems = this.getArguments().getString("diagnosisItems").split("-");

        if(!this.diagnosisItems[0].equals("none"))
        {
            this.diagnosisIntroTextView.setText(this.diagnosisItems[0]);

            for(int i = 1 ; i < this.diagnosisItems.length ; i++)
            {
                this.diagnosisItemsTextView.append("-" + this.diagnosisItems[i] + "\n\n");
            }
        }

        if(this.diagnosisItems[0].equals("none"))
        {
            for(int i = 1 ; i < this.diagnosisItems.length ; i++)
            {
                this.diagnosisIntroTextView.append("-" + this.diagnosisItems[i] + "\n\n");
            }
        }
    }
}
