package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import java.io.File;

public class LoadingFragment extends Fragment {

    private View fragmentView;
    private int dbVersion;

    public static LoadingFragment newInstance()
    {
        LoadingFragment myFragment = new LoadingFragment();

        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.fragmentView = inflater.inflate(R.layout.loading_fragment, container, false);

        return this.fragmentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new VersionAsyncTask(this).execute();
    }

    public int getDbVersion()
    {
        return this.dbVersion;
    }

    private class VersionAsyncTask extends AsyncTask<Void, Integer, Void>
    {
        private Fragment fragment;
        File database = null;

        public VersionAsyncTask(Fragment fragment)
        {
            this.fragment = fragment;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            JSONArray myArray = WebService.SendGetRequest(this.fragment.getActivity(), "//db_versions.json");

            try
            {
                dbVersion = Integer.parseInt(myArray.getJSONObject(0).getString("version"));
            }
            catch(JSONException ex)
            {
                Toast.makeText(this.fragment.getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            database = this.fragment.getActivity().getDatabasePath(AppDB.DATABASE_NAME);

            if(!database.exists())
            {
                SQLiteDatabase db = new AppDB(this.fragment.getActivity(),dbVersion).getWritableDatabase();
                db.close();
                new DBPopulateThread(this.fragment.getActivity(), this.fragment, false).execute();
            }
            else
            {
                Boolean IsUpdated = new AppDB(this.fragment.getActivity(), dbVersion).Updated();

                if(IsUpdated)
                {
                    new DBPopulateThread(this.fragment.getActivity(), this.fragment, true).execute();
                }
                else
                {
                    new LoadingThread(this.fragment).execute();
                }
            }
        }
    }
}
