package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class TickActivity extends Activity implements View.OnClickListener{

    private TickClass selectedTick;
    private TextView nameTextView;
    private TextView tickInfoTextView;
    private Button tickMapButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tick_activity);

        this.selectedTick = (TickClass) getIntent().getParcelableExtra("tick");
        this.nameTextView = (TextView) this.findViewById(R.id.name);
        this.tickInfoTextView = (TextView) this.findViewById(R.id.tick_info);
        this.tickMapButton = (Button) this.findViewById(R.id.btn_tickmapdistribution);
        this.tickMapButton.setOnClickListener(this);

        this.nameTextView.setText(this.selectedTick.GetTickName());
        this.CreateSpannableText(this.tickInfoTextView, "Where Found:");
        this.CreateSpannableText(this.tickInfoTextView, "\n\nComments:");
    }

    private void CreateSpannableText(TextView view, String title)
    {
        SpannableStringBuilder spannable = new SpannableStringBuilder(title);
        spannable.setSpan(new StyleSpan(Typeface.BOLD),0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.append("\n\n");
        spannable.append(this.selectedTick.GetComments());
        view.append(spannable);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, TickMapActivity.class);
        intent.putExtra("mapid", selectedTick.GetTickDestributionImage());
        startActivity(intent);
    }
}
