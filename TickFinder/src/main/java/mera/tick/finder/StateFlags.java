package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
public class StateFlags {

    public static final int[] Id = {

            R.drawable.alabama,
            R.drawable.alaska,
            R.drawable.arizona,
            R.drawable.arkansas,
            R.drawable.california,
            R.drawable.colorado,
            R.drawable.connecticut,
            R.drawable.delaware,
            R.drawable.districtofcolumbia,
            R.drawable.florida,
            R.drawable.georgia,
            R.drawable.hawaii,
            R.drawable.idaho,
            R.drawable.illinois,
            R.drawable.indiana,
            R.drawable.iowa,
            R.drawable.kansas,
            R.drawable.kentucky,
            R.drawable.louisiana,
            R.drawable.maine,
            R.drawable.maryland,
            R.drawable.massachusetts,
            R.drawable.michigan,
            R.drawable.minnesota,
            R.drawable.mississippi,
            R.drawable.missouri,
            R.drawable.montana,
            R.drawable.nebraska,
            R.drawable.nevada,
            R.drawable.newhampshire,
            R.drawable.newjersey,
            R.drawable.newmexico,
            R.drawable.newyork,
            R.drawable.northcarolina,
            R.drawable.northdakota,
            R.drawable.ohio,
            R.drawable.oklahoma,
            R.drawable.oregon,
            R.drawable.pennsylvania,
            R.drawable.rhodeisland,
            R.drawable.southcarolina,
            R.drawable.southdakota,
            R.drawable.tennessee,
            R.drawable.texas,
            R.drawable.utah,
            R.drawable.vermont,
            R.drawable.virginia,
            R.drawable.washington,
            R.drawable.wesvirginia,
            R.drawable.wisconsin,
            R.drawable.wyoming
    };
}