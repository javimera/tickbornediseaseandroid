package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class FragmentSymptomListAdapter extends BaseAdapter {

    private List<String> symptoms;
    private LayoutInflater inflater;

    public FragmentSymptomListAdapter(Context ctx, List<String> s)
    {
        this.symptoms = s;
        this.inflater = LayoutInflater.from(ctx);
    }

    @Override
    public int getCount() {
        return this.symptoms.size();
    }

    @Override
    public String getItem(int i) {
        return this.symptoms.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        SymptomHolder holder = null;

        if(!(view instanceof ViewGroup))
        {
            holder = new SymptomHolder();
            view = inflater.inflate(R.layout.fragment_symptom_list_item, viewGroup, false);

            holder.symptomName = (TextView) view.findViewById(R.id.symptom);

            view.setTag(holder);
        }
        else
        {
            holder = (SymptomHolder) view.getTag();
        }

        holder.symptomName.setText(symptoms.get(i));

        return view;
    }

    private class SymptomHolder
    {
        public TextView symptomName;
    }
}