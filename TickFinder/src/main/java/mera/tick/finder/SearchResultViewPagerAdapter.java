package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class SearchResultViewPagerAdapter extends FragmentStatePagerAdapter {

    private StateClass[] statesChosen;
    public static int SIZE;

    public SearchResultViewPagerAdapter(FragmentManager fm, StateClass[] states) {
        super(fm);
        this.statesChosen = states;
        SIZE = this.statesChosen.length;
    }

    @Override
    public Fragment getItem(int i) {
        return SelectedStateFragment.newInstance(i, this.statesChosen[i]);
    }

    @Override
    public int getCount() {
        return this.statesChosen.length;
    }
}
