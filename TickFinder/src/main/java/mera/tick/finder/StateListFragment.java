package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import static android.view.View.OnClickListener;

public class StateListFragment extends Fragment {

    private View fragmentView;
    private Button viewButton;
    private StateClass[] states;
    private ListView stateList;
    private StateListAdapter adapter;

    public static StateListFragment newInstance(StateClass[] states)
    {
        StateListFragment myFragment = new StateListFragment();
        Bundle args = new Bundle();
        args.putParcelableArray("states", states);
        myFragment.setArguments(args);

        return myFragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.states = (StateClass[]) getArguments().getParcelableArray("states");
        this.adapter = new StateListAdapter(this.states, getActivity());
        this.stateList.setAdapter(adapter);

        this.viewButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!isListNotSelected())
                {
                    getActivity().startActivity(getStatesChecked());
                }
                else
                {
                    ShowAlert();
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.fragmentView = inflater.inflate(R.layout.statelist_fragment, container, false);
        this.viewButton = (Button) this.fragmentView.findViewById(R.id.searchBtn);
        this.stateList = (ListView) this.fragmentView.findViewById(R.id.listview1);

        return this.fragmentView;
    }

    private Intent getStatesChecked()
    {
        Intent intent = new Intent(getActivity(), SearchResultActivity.class);
        int howMany = 0;
        Bundle bundle = new Bundle();

        for(StateClass sc : this.states)
        {
            if(sc.IsChecked())
            {
                bundle.putParcelable("state" + howMany, sc);
                howMany++;
            }
        }

        intent.putExtra("howMany", howMany);
        intent.putExtras(bundle);

        return intent;
    }

    private boolean isListNotSelected()
    {
        for(StateClass sc: this.states)
        {
            if(sc.IsChecked())
                return false;
        }

        return true;
    }

    private void ShowAlert()
    {
        AlertDialog warningMessage = new AlertDialog.Builder(getActivity()).create();
        warningMessage.setTitle("Warning!");
        warningMessage.setMessage("No state has been selected.");
        warningMessage.setIcon(R.drawable.ic_bullet_key_permission);
        warningMessage.setButton(AlertDialog.BUTTON_POSITIVE, "Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        warningMessage.show();
    }
}