package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.os.Parcel;
import android.os.Parcelable;

public class DiseaseClass implements Parcelable {

    private String name;
    private String description;
    private String symptom;
    private String treatment;
    private int Id;
    private TickClass[] ticks;
    private String treatmentLength;
    private String diagnosis;
    private String whereFound;
    private String incubationPeriod;
    private int colorId;

    public static final String TABLE = "diseases";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_SYMPTOMS = "symptoms";
    public static final String COLUMN_TREATMENT = "treatment";
    public static final String COLUMN_TICKS = "tick";
    public static final String COLUMN_LENGTH = "length";
    public static final String COLUMN_DIAGNOSIS= "lab_diagnosis";
    public static final String COLUMN_WHERE_FOUND = "where_found";
    public static final String COLUMN_INCUBATION_PERIOD = "incubation_period";
    public static final String COLUMN_INCIDENCE_COLOR = "statistics";

    public static final String CREATE_STRING =
            "CREATE TABLE " + TABLE + "(" +
                    AppDB.COLUMN_ID + " INTEGER PRIMARY KEY autoincrement," +
                    COLUMN_NAME + " TEXT," +
                    COLUMN_DESCRIPTION + " TEXT," +
                    COLUMN_SYMPTOMS + " TEXT," +
                    COLUMN_TREATMENT + " TEXT," +
                    COLUMN_TICKS + " TEXT," +
                    COLUMN_LENGTH + " TEXT," +
                    COLUMN_DIAGNOSIS + " TEXT," +
                    COLUMN_WHERE_FOUND + " TEXT," +
                    COLUMN_INCUBATION_PERIOD + " TEXT," +
                    COLUMN_INCIDENCE_COLOR + " INTEGER)";

    public DiseaseClass(String name, String desc, String symp, String treat, int id, TickClass[] ticks, String length, String diagnosis, String where, String incubation, int colorId)
    {
        this.name = name;
        this.description = desc;
        this.symptom = symp;
        this.treatment = treat;
        this.Id = id;
        this.ticks = ticks;
        this.treatmentLength = length;
        this.diagnosis = diagnosis;
        this.whereFound = where;
        this.incubationPeriod = incubation;
        this.colorId = colorId;
    }

    public int getId()
    {
        return this.Id;
    }

    public String getName()
    {
        return this.name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getSymptom()
    {
        return this.symptom;
    }

    public String getTreatment()
    {
        return this.treatment;
    }

    public TickClass getTick(int position)
    {
        return this.ticks[position];
    }

    public TickClass[] getAllTicks()
    {
        return this.ticks;
    }

    public String getTreatmentLength()
    {
        return this.treatmentLength;
    }

    public String getDiagnosis()
    {
        return this.diagnosis;
    }

    public String getWhereFound()
    {
        return this.whereFound;
    }

    public String getIncubationPeriod()
    {
        return this.incubationPeriod;
    }

    public int getColorId()
    {
        return this.colorId;
    }

    public DiseaseClass(Parcel parcel)
    {
        this.name = parcel.readString();
        this.description = parcel.readString();
        this.symptom = parcel.readString();
        this.treatment = parcel.readString();
        this.Id = parcel.readInt();

        this.ticks = new TickClass[parcel.readInt()];

        for(int t = 0 ; t < this.ticks.length ; t++)
        {
            this.ticks[t] = TickClass.CREATOR.createFromParcel(parcel);
        }

        this.treatmentLength = parcel.readString();
        this.diagnosis = parcel.readString();
        this.whereFound = parcel.readString();
        this.incubationPeriod = parcel.readString();
        this.colorId = parcel.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeString(this.description);
        parcel.writeString(this.symptom);
        parcel.writeString(this.treatment);
        parcel.writeInt(this.Id);
        parcel.writeInt(this.ticks.length);

        for(TickClass tc: this.ticks)
        {
            tc.writeToParcel(parcel, i);
        }

        parcel.writeString(this.treatmentLength);
        parcel.writeString(this.diagnosis);
        parcel.writeString(this.whereFound);
        parcel.writeString(this.incubationPeriod);
        parcel.writeInt(this.colorId);
    }

    public static final Parcelable.Creator<DiseaseClass> CREATOR = new Parcelable.Creator<DiseaseClass>()
    {
        @Override
        public DiseaseClass createFromParcel(Parcel parcel) {
            return new DiseaseClass(parcel);
        }

        @Override
        public DiseaseClass[] newArray(int i) {
            return new DiseaseClass[i];
        }
    };
}
