package mera.tick.finder;

/**
 * Created by Javi on 7/3/13.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;

public class AppDB extends SQLiteOpenHelper {

    private Context context;

    public static final String DATABASE_NAME = "tickborneapp.db";
    public static final String COLUMN_ID = "id";
    private boolean IsUpdated;

    private SQLiteDatabase db;

    public AppDB(Context ctx, int version)
    {
        super(ctx, DATABASE_NAME, null, version);
        this.context = ctx;
        this.IsUpdated = false;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(StateClass.CREATE_STRING);
        sqLiteDatabase.execSQL(DiseaseClass.CREATE_STRING);
        sqLiteDatabase.execSQL(IncidenceClass.CREATE_STRING);
        sqLiteDatabase.execSQL(TickClass.CREATE_STRING);

        this.IsUpdated = true;

        Toast.makeText(this.context, "Created", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + StateClass.TABLE);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DiseaseClass.TABLE);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + IncidenceClass.TABLE);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TickClass.TABLE);

        Toast.makeText(this.context, "Upgrading", Toast.LENGTH_LONG).show();
        this.onCreate(sqLiteDatabase);
    }

    public boolean Updated()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        db.close();

        return this.IsUpdated;
    }
    public void InsertDiseases(JSONArray resp)
    {
        this.db = this.getWritableDatabase();

        try
        {
            for(int i = 0 ; i < resp.length() ; i++)
            {
                ContentValues values = new ContentValues();
                values.put(DiseaseClass.COLUMN_NAME, resp.getJSONObject(i).getString(DiseaseClass.COLUMN_NAME));
                values.put(DiseaseClass.COLUMN_DESCRIPTION, resp.getJSONObject(i).getString(DiseaseClass.COLUMN_DESCRIPTION));
                values.put(DiseaseClass.COLUMN_SYMPTOMS, resp.getJSONObject(i).getString(DiseaseClass.COLUMN_SYMPTOMS));
                values.put(DiseaseClass.COLUMN_TREATMENT, resp.getJSONObject(i).getString(DiseaseClass.COLUMN_TREATMENT));
                values.put(DiseaseClass.COLUMN_TICKS, resp.getJSONObject(i).getString(DiseaseClass.COLUMN_TICKS));
                values.put(DiseaseClass.COLUMN_LENGTH, resp.getJSONObject(i).getString(DiseaseClass.COLUMN_LENGTH));
                values.put(DiseaseClass.COLUMN_DIAGNOSIS, resp.getJSONObject(i).getString(DiseaseClass.COLUMN_DIAGNOSIS));
                values.put(DiseaseClass.COLUMN_WHERE_FOUND, resp.getJSONObject(i).getString(DiseaseClass.COLUMN_WHERE_FOUND));
                values.put(DiseaseClass.COLUMN_INCUBATION_PERIOD, resp.getJSONObject(i).getString(DiseaseClass.COLUMN_INCUBATION_PERIOD));
                values.put(DiseaseClass.COLUMN_INCIDENCE_COLOR, Integer.parseInt(resp.getJSONObject(i).getString(DiseaseClass.COLUMN_INCIDENCE_COLOR)));

                this.db.insert(DiseaseClass.TABLE, null, values);
            }
        }
        catch(JSONException ex)
        {
            Toast.makeText(this.context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        catch(NullPointerException ex)
        {
            Toast.makeText(this.context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        finally
        {
            this.db.close();
        }
    }

    public void InsertStates(JSONArray resp)
    {
        this.db = this.getWritableDatabase();

        try
        {
            for(int i = 0 ; i < resp.length() ; i++)
            {
                ContentValues values = new ContentValues();

                values.put(StateClass.COLUMN_NAME, resp.getJSONObject(i).getString(StateClass.COLUMN_NAME));

                db.insert(StateClass.TABLE, null, values);
            }
        }
        catch(JSONException ex)
        {
            Toast.makeText(this.context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        catch(NullPointerException ex)
        {
            Toast.makeText(this.context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        finally
        {
            this.db.close();
        }
    }

    public void InsertTicks(JSONArray resp)
    {
        this.db = this.getWritableDatabase();

        try
        {
            for(int i = 0 ; i < resp.length() ; i++)
            {
                ContentValues values = new ContentValues();

                values.put(TickClass.COLUMN_NAME, resp.getJSONObject(i).getString(TickClass.COLUMN_NAME));
                values.put(TickClass.COLUMN_SCI_NAME, resp.getJSONObject(i).getString(TickClass.COLUMN_SCI_NAME));
                values.put(TickClass.COLUMN_COMMENTS, resp.getJSONObject(i).getString(TickClass.COLUMN_COMMENTS));
                values.put(TickClass.COLUMN_WHERE_FOUND, resp.getJSONObject(i).getString(TickClass.COLUMN_WHERE_FOUND));

                db.insert(TickClass.TABLE, null, values);
            }
        }
        catch(JSONException ex)
        {
            Toast.makeText(this.context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        catch(NullPointerException ex)
        {
            Toast.makeText(this.context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        finally
        {
            this.db.close();
        }
    }

    public void InsertIncidence(JSONArray resp)
    {
        this.db = this.getWritableDatabase();

        try
        {
            for(int i = 0 ; i < resp.length() ; i++)
            {
                ContentValues values = new ContentValues();

                values.put(IncidenceClass.COLUMN_ID_DISEASE, Integer.parseInt(resp.getJSONObject(i).getString(IncidenceClass.COLUMN_ID_DISEASE)));
                values.put(IncidenceClass.COLUMN_ID_STATE, Integer.parseInt(resp.getJSONObject(i).getString(IncidenceClass.COLUMN_ID_STATE)));
                values.put(IncidenceClass.COLUMN_VALUE, resp.getJSONObject(i).getString(IncidenceClass.COLUMN_VALUE));

                this.db.insert(IncidenceClass.TABLE, null, values);
            }
        }
        catch(JSONException ex)
        {
            Toast.makeText(this.context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        catch(NullPointerException ex)
        {
            Toast.makeText(this.context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        finally
        {
            this.close();
        }
    }
}